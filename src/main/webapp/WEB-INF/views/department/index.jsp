<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1> Department List</h1>
	<ol>
		<c:forEach items="${departmentList}" var="department">
			<li>
				(${department.getId()})
				${department.getName()} 
				<a href="/department/edit/${department.getId()}">edit</a> 
				<a href="/department/delete/${department.getId()}">delete</a>  
			</li>
		</c:forEach>
	</ol>
	<a href="/department/add">Add new</a> 
</body>
</html>