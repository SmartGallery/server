<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SmartGallry start page</title>
</head>
<body>
	<h1>Let`s get party started :)</h1>
	<p>${message}</p>
	<ol>
		<c:forEach items="${songs}" var="song">
			<li>${song}</li>
		</c:forEach>
	</ol>
	<h2>Employees</h2>
	<ol>
		<c:forEach items="${employeeList}" var="employee">
			<li>${employee.getName()}</li>
		</c:forEach>
	</ol>
</body>
</html>