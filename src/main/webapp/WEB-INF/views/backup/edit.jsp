<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form:form action="/backup/edit/${backup.getId()}" modelAttribute="backup" method="post">
	Name: <form:input path="name"/>
	<form:errors path="name"/>
	<br />
	Last_date (dd-MM-yyyy): 
	<form:input path="last_date" value="${backup.getLast_date_as_string()}"  />
	<form:errors path="last_date"/>
	<br />
	Status: 
	<form:select path="status" items="${statusList}" itemLabel="key" />
	<br />
	Frequency: 
	<form:select path="frequency" items="${frequencyList}" itemLabel="key" />
	<br />
	Configuration: 
	<form:input path="configuration"/>
	<form:errors path="configuration"/>
	<br />
			
	<br /><br /><input type="submit" value="Save" /> 
</form:form>

</body>
</html>