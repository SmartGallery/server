<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1> backup view</h1>
<c:forEach items="${backupList}" var="backup">
			<li>
				(${backup.getId()}) 
				Configuration: ${backup.getConfiguration()} 
				Name: ${backup.getName()}
				Last Date: ${backup.getLast_date()}
				
				<a href="/backup/edit/${backup.getId()}">edit</a> 
				<a href="/backup/delete/${backup.getId()}">delete</a>
			</li>
		</c:forEach>
	</ol>
	<a href="/backup/edit/0">Add new</a>

</body>
</html>