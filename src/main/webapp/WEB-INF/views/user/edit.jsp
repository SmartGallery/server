<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form:form action="/user/edit/${user.getId()}" modelAttribute="user" method="post">
	Name: <form:input path="login"/>
	<form:errors path="login"/>
	<br />
	Password: 
	<form:password path="password"  />
	<form:errors path="password"/>
	<br />
	Password confirmation: 
	<form:password path="passwordConfirmation"/>
	<form:errors path="passwordConfirmation"/>
	<br />
	Type: 
	<form:select path="type" items="${typeList}" itemLabel="label" />
	<br />
	Enabled: 
	<form:checkbox path="enabled" />
	<br />
	<br />
	
	Teams: 
	<br />
	<form:select path="teams" items="${teamList}" itemLabel="name" itemValue="id" multiple="true" />
	<form:errors path="teams"/>
	<br />
		
	<br /><br /><input type="submit" value="Save" /> 
</form:form>
</body>
</html>