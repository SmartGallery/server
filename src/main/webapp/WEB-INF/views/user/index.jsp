<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1> User List</h1>
	<ol>
		<c:forEach items="${userList}" var="user">
			<li>
				(${user.getId()})
				Login: ${user.getLogin()};
				Type: ${user.getType().getLabel()};
				Enabled: ${user.getEnabled()};
				<a href="/user/edit/${user.getId()}">edit</a> 
				<a href="/user/delete/${user.getId()}">delete</a>
				<a href="/user/block/${user.getId()}">block</a>
				<a href="/user/unblock/${user.getId()}">unblock</a>  
			</li>
		</c:forEach>
	</ol>
	<a href="/user/add">Add new</a> 
</body>
</html>