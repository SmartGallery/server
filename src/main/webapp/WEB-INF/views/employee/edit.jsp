<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Formularz Employee</h1>
	<form:form action="/employee/edit/${employee.getId()}" modelAttribute="employee" method="post">
	Name: <form:input path="name"/>
	<form:errors path="name"/>
	<br />
	Department: 
	<form:select path="department.id" items="${departmentList}" itemLabel="name" itemValue="id"  />
		
	<br /><br /><input type="submit" value="Save" /> 
	</form:form>		
</body>
</html>