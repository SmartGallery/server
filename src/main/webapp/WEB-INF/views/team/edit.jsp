<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1> TEAM EDIT VIEWS </h1>
<form:form action="/team/edit/${team.getId()}" modelAttribute="team" method="post">
	
	Administrator: 
	<form:select path="admin" items="${userList}" itemLabel="login" itemValue="id" />
	<form:errors path="admin"/>
	<br />
	
	
	Users: 
	<form:select path="users" items="${userList}" itemLabel="login" itemValue="id" multiple="true" />
	<form:errors path="users"/>
	<br />
	
	
	Name: <form:input path="name"/>
	<form:errors path="name"/>
		
	<br /><br /><input type="submit" value="Save" /> 
	</form:form>		
</body>
</html>