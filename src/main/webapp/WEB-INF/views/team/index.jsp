<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>TEAM Index VIEWS</h1>
	<ol>
		<c:forEach items="${teamList}" var="team">
			<li>
				(${team.getId()}) 
				Administrator: ${team.getAdmin().getLogin()} 
				team name: ${team.getName()}
				
				<a href="/team/edit/${team.getId()}">edit</a> 
				<a href="/team/delete/${team.getId()}">delete</a>
			</li>
		</c:forEach>
	</ol>
	<a href="/team/edit/0">Add new</a>

</body>
</html>