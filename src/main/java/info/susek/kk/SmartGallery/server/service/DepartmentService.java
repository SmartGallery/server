package info.susek.kk.SmartGallery.server.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import info.susek.kk.SmartGallery.server.model.Department;
import info.susek.kk.SmartGallery.server.model.DepartmentDAO;

@Service
public class DepartmentService {

	@Autowired
	private DepartmentDAO departmentDAO;

	public List<Department> allDepartment() {
		List<Department> departmentList = new ArrayList<Department>();

		Iterable<Department> daoDepartmentList = departmentDAO.findAll();
		Iterator<Department> daoDepartmentIterator = daoDepartmentList.iterator();

		while (daoDepartmentIterator.hasNext()) {
			departmentList.add(daoDepartmentIterator.next());
		}
		
		return departmentList;
	}

	public void deleteDepartment(Long id) {
		departmentDAO.delete(id);
		
	}

	public String getDepartmentName(Long id) {
		Department department = departmentDAO.findOne(id);
		return department.getName();
	}

	public Department getDepartment(Long id) {
		return departmentDAO.findOne(id);
	}

	public void saveDepartment(Department departemnt) {
		departmentDAO.save(departemnt);
	}
}
