package info.susek.kk.SmartGallery.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import info.susek.kk.SmartGallery.server.model.Team;
import info.susek.kk.SmartGallery.server.model.User;
import info.susek.kk.SmartGallery.server.model.UserDTO;
import info.susek.kk.SmartGallery.server.model.UserType;
import info.susek.kk.SmartGallery.server.model.UserValidator;
import info.susek.kk.SmartGallery.server.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserValidator userValidator;

	
	@RequestMapping("/")
	public String index(Model model) {

		List<User> userList = userService.getUsers();
		model.addAttribute("userList", userList);

		return "user/index";
	}

	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) {

		String userLogin = userService.getUserLogin(id);

		userService.deleteUser(id);
		model.addAttribute("userLogin", userLogin);

		return "redirect:/user/";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") Long id, Model model) {

		User user;
		UserDTO userDTO;
		
		if (id == 0) {
			user = new User();
			user.setType(UserType.USER);
			user.setEnabled(1);
		} else {
			user = userService.getUser(id);
		}

		userDTO = new UserDTO(user);
		model.addAttribute("user", userDTO);
		model.addAttribute("typeList", UserType.values());

		List<Team> teamList = userService.teamList();
		model.addAttribute("teamList", teamList);
		
		return "user/edit";
	}

	@RequestMapping(value = "edit/{id}", method = RequestMethod.POST)
	public String save(@PathVariable("id") Long id, @ModelAttribute("user") @Valid UserDTO userDTO, BindingResult br,
			Model model) {
		
		model.addAttribute("typeList", UserType.values());
		
		String view = "user/edit";
		userValidator.validate(userDTO, br);

		if (!br.hasErrors()){
			
			userDTO.setId(id);
			User user = userDTO.getUser();
			userService.saveUser(user);
			
			view = "redirect:/user/";
		}

		return view;
	}

	@RequestMapping("/add")
	public String add(Model model) {
		return edit(0L, model);
	}
	
	@RequestMapping("/block/{id}")
	public String block(@PathVariable("id") Long id, Model model) {

		User user = userService.getUser(id);
		userService.blockUser(user);
		
		return "redirect:/user/";
	}
	
	@RequestMapping("/unblock/{id}")
	public String unblock(@PathVariable("id") Long id, Model model) {

		User user = userService.getUser(id);
		userService.unblockUser(user);
		
		return "redirect:/user/";
	}


}
