package info.susek.kk.SmartGallery.server;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndViewDefiningException;

import info.susek.kk.SmartGallery.server.model.Department;
import info.susek.kk.SmartGallery.server.model.Team;
import info.susek.kk.SmartGallery.server.model.TeamDAO;
import info.susek.kk.SmartGallery.server.model.TeamDTO;
import info.susek.kk.SmartGallery.server.model.TeamValidator;
import info.susek.kk.SmartGallery.server.model.User;
import info.susek.kk.SmartGallery.server.model.UserDTO;
import info.susek.kk.SmartGallery.server.model.UserType;
import info.susek.kk.SmartGallery.server.service.UserService;

@Controller
@RequestMapping("/team")
public class TeamController {

	@Autowired
	UserService userService;
	
	@Autowired
	TeamDAO teamDAO;
	
	@Autowired
	private TeamValidator teamValidator;
	
	@RequestMapping("/")
	public String index(Model model) {
	
		
		List<Team> teamList = userService.teamList();
		model.addAttribute("teamList", teamList);
		
		return "team/index";
	}
	
	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id) {

		userService.deleteTeam(id);
			
		return "redirect:/team/";
	}
	
	@RequestMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) {
		
		Team team;
		List <User> userList;
			
		if (id == 0) {
			team = new Team();
			userList = userService.getUsersWithChoose();
						
		} else {
			team = userService.getTeam(id);
			team.getAdmin().getId();
			team.getUsers().iterator().hasNext();
			
			userList = userService.getUsers();
		}

		TeamDTO teamDTO = new TeamDTO(team);
		model.addAttribute("team", teamDTO);
		model.addAttribute("userList", userList);
	

		return "team/edit";
	}
	
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String save(@PathVariable("id") Long id,@ModelAttribute("team") @Valid TeamDTO teamDTO, BindingResult br, Model model) {

		String view = "team/edit";
		teamValidator.validate(teamDTO, br);

		if (!br.hasErrors()){
			
			teamDTO.setId(id);
			Team team = teamDTO.getTeam();
			userService.saveTeam(team);
			
			view = "redirect:/team/";
		}
		else
		{
			List <User> userList = userService.getUsers();
			model.addAttribute("userList", userList);
		}

		return view;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String add(Model model) {

		Team team = new Team();
		model.addAttribute("teamDTO", team);
		
		return "team/edit/0";
	}

	
	
	
	
	
	
}
