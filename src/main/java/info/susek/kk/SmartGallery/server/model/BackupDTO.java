package info.susek.kk.SmartGallery.server.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BackupDTO {


	@Id
	@GeneratedValue
	private long id;
	
	@NotNull
	private String name;
	private BackupStatus status;
	
	private Date last_date;
	
	@NotNull
	@Size(min=2, max= 25)
	private String configuration;
	private BackupFrequency frequency;
	
	
	public BackupDTO(){
		
	}

	public BackupDTO(Backup backup) {
		
		this.id = backup.getId();
		this.name = backup.getName();
		this.status = backup.getStatus();
		this.last_date = backup.getLast_date();
		this.configuration = backup.getConfiguration();
		this.frequency = backup.getFrequency();
		
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the last_date
	 */
	public Date getLast_date() {
		return last_date;
	}
	
	public String getLast_date_as_string()
	{
		if(null == last_date){
			return "";
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		//return last_date.toString(formatter);
		return formatter.format(last_date);
	}

	/**
	 * @param last_date the last_date to set
	 */
	public void setLast_date(Date last_date) {
		this.last_date = last_date;
	}

	/**
	 * @return the configuration
	 */
	public String getConfiguration() {
		return configuration;
	}

	/**
	 * @param configuration the configuration to set
	 */
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public Backup getBackup() {
		
		Backup backup = new Backup();
		backup.setId(id);
		backup.setName(name);
		backup.setConfiguration(configuration);
		backup.setLast_date(last_date);
		backup.setFrequency(frequency);
		backup.setStatus(status);
		
		return backup;
	}

	/**
	 * @return the status
	 */
	public BackupStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(BackupStatus status) {
		this.status = status;
	}

	/**
	 * @return the frequency
	 */
	public BackupFrequency getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency the frequency to set
	 */
	public void setFrequency(BackupFrequency frequency) {
		this.frequency = frequency;
	}
	
	
	
}
