package info.susek.kk.SmartGallery.server.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import info.susek.kk.SmartGallery.server.service.BackupService;

@Component
public class StringToDateConventer implements Converter<Object, Date> {

	@Autowired
	BackupService backupService;
	
	@Override
	public Date convert(Object source) {
		
		Date date = new Date();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String dateInString = (String)source;

        try {

            date = formatter.parse(dateInString);
            /*System.out.println(date);
            System.out.println(formatter.format(date));*/

        } catch (ParseException e) {
            e.printStackTrace();
        }
		return date;
	}

}
