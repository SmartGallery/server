package info.susek.kk.SmartGallery.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import info.susek.kk.SmartGallery.server.model.Department;
import info.susek.kk.SmartGallery.server.model.DepartmentDTO;
import info.susek.kk.SmartGallery.server.model.Employee;
import info.susek.kk.SmartGallery.server.service.DepartmentService;
import info.susek.kk.SmartGallery.server.service.EmployeeService;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private DepartmentService departmentService;

	@RequestMapping("/")
	public String index(Model model) {

		List<Employee> employeeList = employeeService.allEmployees();
		model.addAttribute("employeeList", employeeList);

		return "employee/index";
	}

	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id) {

		employeeService.deleteEmployee(id);
		
		return "redirect:/employee/";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") Long id, Model model) {
		
		Employee employee;
		
		if (id == 0) {
			employee = new Employee();
		}
		else{
			employee = employeeService.getEmployee(id);
			
		}
		
		List<Department> departmentList = departmentService.allDepartment();
		model.addAttribute("departmentList", departmentList);
		model.addAttribute("employee", employee);
		
		
		// employeeService.employee(employee.getName(),employee.getDepartment().getName());

		return "employee/edit";

	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String save(@PathVariable("id") Long id, Model model, Employee employee) {

		employeeService.update(employee);

		return "redirect:/employee/";
	}

}
