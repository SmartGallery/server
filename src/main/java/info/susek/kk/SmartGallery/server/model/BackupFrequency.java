package info.susek.kk.SmartGallery.server.model;

public enum BackupFrequency {

	DAILY("daily","1d"),
	EVERY2DAYS("every two days","2d"),
	WEEKLY("weekly","1w"),
	MANUAL("manual","manual");
	
	final String key;
	final String value;
	
	private BackupFrequency(String key,String value)
	{
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}
	public String getValue(){
		return value;
	}
	
	
}
