package info.susek.kk.SmartGallery.server.model;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

@Transactional
@Repository
public class EmployeeDAO {

	@PersistenceContext
	private EntityManager em;

	public List<Employee> getAll() {
		Query q = em.createQuery("SELECT e FROM Employee e");

		return q.getResultList();
	}

	public Employee add(Employee e) {
		return em.merge(e);
	}

	public Employee findOne(Long id) {

		Query q = em.createQuery("SELECT e FROM Employee e WHERE id = :id");
		q.setParameter("id", id);
		return (Employee) q.getSingleResult();
	}

	public void deleteEmployee(Long id) {
		
		Query q = em.createQuery("DELETE FROM Employee WHERE id = :id");
		q.setParameter("id", id);
		q.executeUpdate();
	}

	public Employee save(Employee employee) {
		return em.merge(employee);
		
	}

	public List<Employee> getByDepartment(long department_id) {
		Query q = em.createQuery("SELECT e FROM Employee e WHERE department_id = :department_id");
		q.setParameter("department_id", department_id);

		return q.getResultList();
	}
	
}
