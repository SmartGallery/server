package info.susek.kk.SmartGallery.server.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Backup {

	
	@Id
	@GeneratedValue
	private long id;
	
	@NotNull
	private String name;
	@Enumerated(EnumType.STRING)
	private BackupStatus status;
	
	private Date last_date;
	
	@NotNull
	@Size(min=2, max= 25)
	private String configuration;
	@Enumerated(EnumType.STRING)
	private BackupFrequency frequency;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the last_date
	 */
	public Date getLast_date() {
		return last_date;
	}

	/**
	 * @param last_date the last_date to set
	 */
	public void setLast_date(Date last_date) {
		this.last_date = last_date;
	}

	/**
	 * @return the configuration
	 */
	public String getConfiguration() {
		return configuration;
	}

	/**
	 * @param configuration the configuration to set
	 */
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	/**
	 * @return the status
	 */
	public BackupStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(BackupStatus status) {
		this.status = status;
	}

	/**
	 * @return the frequency
	 */
	public BackupFrequency getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency the frequency to set
	 */
	public void setFrequency(BackupFrequency frequency) {
		this.frequency = frequency;
	}
	
	
	
}
