package info.susek.kk.SmartGallery.server.model;

public enum UserType {

	USER("User"),
	ADMIN("Administrator");
	
	final String label;
	
	UserType(String label)
	{
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}
