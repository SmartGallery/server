package info.susek.kk.SmartGallery.server.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Album {

	@Id
	@GeneratedValue
	private long id;
	
	@JsonIgnore
	@ManyToOne
	private User user;
	private String name;
	
	@Enumerated(EnumType.STRING)
	private AlbumVisibility visibility;
	private String description;
	private Date date;
	private Date date_modification;
	private String tags;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "AlbumPhoto", 
				joinColumns = {@JoinColumn(name = "album_id")},
				inverseJoinColumns = {@JoinColumn(name = "photo_id")})
	private Set<Photo> photos = new HashSet<Photo>();
	
	
	
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the visibility
	 */
	public AlbumVisibility getVisibility() {
		return visibility;
	}
	/**
	 * @param visibility the visibility to set
	 */
	public void setVisibility(AlbumVisibility visibility) {
		this.visibility = visibility;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the date_modification
	 */
	public Date getDate_modification() {
		return date_modification;
	}
	/**
	 * @param date_modification the date_modification to set
	 */
	public void setDate_modification(Date date_modification) {
		this.date_modification = date_modification;
	}
	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the photos
	 */
	public Set<Photo> getPhotos() {
		return photos;
	}
	/**
	 * @param photos the photos to set
	 */
	public void setPhotos(Set<Photo> photos) {
		this.photos = photos;
	}
}
