package info.susek.kk.SmartGallery.server.model;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class UserDTO {

private long id;
	
	@NotNull	
	@Size(min=3, max=255)
	private String login;
	

	private String password;
	
	private String passwordConfirmation;
			
	private UserType type = UserType.USER;
	
	private boolean enabled = false;
	
	private Set<Team> teams = new HashSet<Team>();
	
	public UserDTO() {
	
	}

	public UserDTO(String login) {
		this.login = login;
	}
	
	public UserDTO(User user) {
		this.id = user.getId();
		this.login = user.getLogin();
		this.type = user.getType();
		this.enabled = (user.getEnabled() == 1 ? true : false);
		this.teams = user.getTeams();
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param name the name to set
	 */
	public void setLogin(String login) {
		this.login = login;
	} 
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", login=" + login + "]";
	}
	
	public User getUser (){
		User user = new User();
		user.setId(id);
		user.setLogin(login);
		user.setType(type);
		user.setTeams(teams);
		
		ShaPasswordEncoder encoder = new ShaPasswordEncoder();
		
		user.setPassword(encoder.encodePassword(password, null));
		user.setEnabled((enabled?1:0));
		return user;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the type
	 */
	public UserType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(UserType type) {
		this.type = type;
	}

	/**
	 * @return the enabled
	 */
	public boolean getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		
		if(enabled != null){
			this.enabled = enabled;
		}
		else{
			enabled = false;
		}
	}

	/**
	 * @return the passwordConfirmation
	 */
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	/**
	 * @param passwordConfirmation the passwordConfirmation to set
	 */
	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

	/**
	 * @return the teams
	 */
	public Set<Team> getTeams() {
		return teams;
	}

	/**
	 * @param teams the teams to set
	 */
	public void setTeams(Set<Team> teams) {
		this.teams = teams;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}

