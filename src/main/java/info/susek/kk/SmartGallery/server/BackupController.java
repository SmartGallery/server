package info.susek.kk.SmartGallery.server;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import info.susek.kk.SmartGallery.server.model.Backup;
import info.susek.kk.SmartGallery.server.model.BackupDTO;
import info.susek.kk.SmartGallery.server.model.BackupFrequency;
import info.susek.kk.SmartGallery.server.model.BackupStatus;
import info.susek.kk.SmartGallery.server.model.BackupValidator;
import info.susek.kk.SmartGallery.server.model.Team;
import info.susek.kk.SmartGallery.server.model.User;
import info.susek.kk.SmartGallery.server.model.UserDTO;
import info.susek.kk.SmartGallery.server.model.UserType;
import info.susek.kk.SmartGallery.server.service.BackupService;

@Controller
@RequestMapping("/backup")
public class BackupController {

	@Autowired
	BackupService backupService;
	
	@Autowired
	BackupValidator backupValidator;
	

	@RequestMapping("/")
	public String index(Model model) {
	
		
		List<Backup> backupList = backupService.getBackups();
		model.addAttribute("backupList", backupList);
		
		return "backup/index";
	}
	
	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id) {

		backupService.deleteBackup(id);

		return "redirect:/backup/";
	}
	

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") Long id, Model model) {

		Backup backup;
		BackupDTO backupDTO;
		
		
		
		if (id == 0) {
			backup = new Backup();
			//buckup.setType(UserType.USER);
			//user.setEnabled(1);
		} else {
			backup = backupService.getBackup(id);
		}

		backupDTO = new BackupDTO(backup);
		model.addAttribute("backup", backupDTO);
		model.addAttribute("statusList", BackupStatus.values());
		model.addAttribute("frequencyList", BackupFrequency.values());
		
		return "backup/edit";
	}

	@RequestMapping(value = "edit/{id}", method = RequestMethod.POST)
	public String save(@PathVariable("id") Long id, @ModelAttribute("backup") @Valid BackupDTO backupDTO, BindingResult br,
			Model model) {
		
		//model.addAttribute("typeList", UserType.values());
		
		String view = "backup/edit";
		backupValidator.validate(backupDTO, br);

		if (!br.hasErrors()){
			
			backupDTO.setId(id);
			Backup backup = backupDTO.getBackup();
			backupService.saveBackup(backup);
			
			view = "redirect:/backup/";
		}

		return view;
	}

	@RequestMapping("/add")
	public String add(Model model) {
		return edit(0L, model);
	}
	
}
