package info.susek.kk.SmartGallery.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import info.susek.kk.SmartGallery.server.model.Album;
import info.susek.kk.SmartGallery.server.model.Photo;
import info.susek.kk.SmartGallery.server.service.PhotoAlbumService;

@RequestMapping("/api")
@RestController
public class PhotoApiController {

	@Autowired
	private PhotoAlbumService photoAlbumService;

	@RequestMapping(value="/photo", method = RequestMethod.GET )
	public List<Photo> getPhotos (){
		
		List <Photo> photos = photoAlbumService.getPhotos();
		
		return photos;
	}
	
	
	
	
}
