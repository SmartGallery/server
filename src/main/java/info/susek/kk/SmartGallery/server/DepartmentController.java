package info.susek.kk.SmartGallery.server;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.annotation.JsonCreator.Mode;

import info.susek.kk.SmartGallery.server.model.Department;
import info.susek.kk.SmartGallery.server.model.DepartmentDTO;
import info.susek.kk.SmartGallery.server.service.DepartmentService;
import info.susek.kk.SmartGallery.server.service.EmployeeService;

@Controller
@RequestMapping("/department")
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private EmployeeService employeeService;

	@RequestMapping("/")
	public String index(Model model) {

		List<Department> departmentList = departmentService.allDepartment();
		model.addAttribute("departmentList", departmentList);

		return "department/index";
	}

	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) {

		String departmentName = departmentService.getDepartmentName(id);

		departmentService.deleteDepartment(id);
		model.addAttribute("departmentName", departmentName);

		return "department/delete";
	}

	@RequestMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) {

		Department department = departmentService.getDepartment(id);
		model.addAttribute("departemntDTO", department);
		model.addAttribute("employeeList", employeeService.allDepartmentEmployees(department.getId()));
		return "department/edit";
	}

	@RequestMapping(value = "/edit-do", method = RequestMethod.POST)
	public String editDo(@ModelAttribute("departemntDTO") @Valid DepartmentDTO departemntDTO, BindingResult br,
			Model model) {

		String view = "";

		if (br.hasErrors()) {
			// model.addAttribute("departemntDTO", departemntDTO);
			view = "department/edit";

		} else {

			Department department = departemntDTO.getDepartment();

			model.addAttribute("department", department);
			departmentService.saveDepartment(department);

			view = "department/edit-do";
		}

		return view;
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String add(Model model) {

		Department department = new Department();
		model.addAttribute("departemntDTO", department);
		return "department/edit";
	}

}
