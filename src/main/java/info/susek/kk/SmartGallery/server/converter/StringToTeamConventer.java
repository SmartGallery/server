package info.susek.kk.SmartGallery.server.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import info.susek.kk.SmartGallery.server.model.Team;
import info.susek.kk.SmartGallery.server.service.UserService;

@Component
public class StringToTeamConventer implements Converter<Object, Team> {

	@Autowired
	UserService userService;
	
	@Override
	public Team convert(Object source) {
		
		Long id = Long.parseLong((String) source);
		Team team = userService.getTeam(id);
			
		return team;
	}

	
}
