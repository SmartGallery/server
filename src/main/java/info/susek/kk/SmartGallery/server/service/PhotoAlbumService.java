package info.susek.kk.SmartGallery.server.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import info.susek.kk.SmartGallery.server.model.Album;
import info.susek.kk.SmartGallery.server.model.AlbumDAO;
import info.susek.kk.SmartGallery.server.model.Photo;
import info.susek.kk.SmartGallery.server.model.PhotoDAO;

@Service
public class PhotoAlbumService {

	@Autowired
	private AlbumDAO albumDAO;
	
	@Autowired
	private PhotoDAO photoDAO;
	
	
	public List<Album> getAlbums() {

		List<Album> albumList = new ArrayList<Album>();

		Iterable<Album> daoAlbumList = albumDAO.findAll();
		Iterator<Album> daoAlbumIterator = daoAlbumList.iterator();

		while (daoAlbumIterator.hasNext()) {
			albumList.add(daoAlbumIterator.next());
		}

		return albumList;
	}


	public Album getAlbum(Long id) {
		
		Album album = albumDAO.findOne(id);
		album.getPhotos().size();
		return album;
	}


	public List<Photo> getPhotos() {
		
		List<Photo> photoList = new ArrayList<Photo>();

		Iterable<Photo> daoPhotoList = photoDAO.findAll();
		Iterator<Photo> daoPhotoIterator = daoPhotoList.iterator();

		while (daoPhotoIterator.hasNext()) {
			photoList.add(daoPhotoIterator.next());
		}

		return photoList;
	}
	
}
