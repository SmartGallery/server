package info.susek.kk.SmartGallery.server.model;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

	@Override
	public boolean supports(Class clazz) {
		return UserDTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object userDTOobject, Errors errors) {
		
		UserDTO userDTO = (UserDTO) userDTOobject;
		
		if(userDTO.getId()==0 && userDTO.getPassword().length()==0){
			
			errors.rejectValue("password", "user.password_empty", "Password can`t be blank");
		}
		
		if(!userDTO.getPassword().equals(userDTO.getPasswordConfirmation())){
			errors.rejectValue("passwordConfirmation", "user.password_not_match", "Password does not match");
		}
		else{
			if(userDTO.getPassword().length() > 0 && 
					(userDTO.getPassword().length()<6 || userDTO.getPassword().length()> 15 )){
				errors.rejectValue("password", "user.password_incorect", "Password lenght must be between 6 and 15");
			}
		}
		
	}

}
