package info.susek.kk.SmartGallery.server.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DepartmentDTO {

	private long id;
	
	@NotNull	
	@Size(min=3, max=255)
	private String name;
	
	public DepartmentDTO() {
	
	}

	public DepartmentDTO(String name) {
		this.name = name;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	} 
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + "]";
	}
	
	public Department getDepartment (){
		Department department = new Department();
		department.setId(id);
		department.setName(name);
		return department;
	}
}
