package info.susek.kk.SmartGallery.server;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import info.susek.kk.SmartGallery.server.model.Department;
import info.susek.kk.SmartGallery.server.model.DepartmentDAO;
import info.susek.kk.SmartGallery.server.model.Employee;
import info.susek.kk.SmartGallery.server.model.EmployeeDAO;

@Controller
public class IndexController {
	
	@Autowired
	private DepartmentDAO departmentDAO;
	@Autowired
	private EmployeeDAO employeeDAO;

	@RequestMapping("/")
	public String index(Model model){
		
		Department hr = new Department("HR");
		
		hr = departmentDAO.save(hr);
		
		Department it = new Department("IT");
		
		it = departmentDAO.save(it);
		
		Employee e = new Employee("John", it);
		employeeDAO.add(e);
		
		List <Employee> employeeList = employeeDAO.getAll();
		
		
		List <Department> departments = (List<Department>) departmentDAO.findAll();
				
	
		
		
		List <String> list = new ArrayList<String>();
		list.add("Again");
		list.add("Fly Away");
		list.add("Everything");
		list.add("Dancing til down");
		model.addAttribute("songs", departments);
		
		model.addAttribute("message", "This is an example");
		model.addAttribute("employeeList", employeeList);
		
		return "index";
	}
	@RequestMapping("/login")
	public String login(Model model){
		
		return "login";
	}
	
	@Secured({"ROLE_USER"})
	@RequestMapping("/welcome")
	public String welcome(Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
              UserDetails userDetail = (UserDetails) auth.getPrincipal();
              model.addAttribute("logged_username", userDetail.getUsername());
        }
        else{
		model.addAttribute("logged_username", "not logged");
        }
		return "welcome";
		
	}
}
