package info.susek.kk.SmartGallery.server;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import info.susek.kk.SmartGallery.server.service.EmployeeService;
import info.susek.kk.SmartGallery.server.model.Employee;

@RequestMapping("/api")
@RestController
public class ApiController {

	 @Autowired
	 EmployeeService employeeService;

	 @RequestMapping(value="/employees", method=RequestMethod.GET)
	 public List<Employee> getEmployees(){
		 return employeeService.allEmployees();
	 }
}
