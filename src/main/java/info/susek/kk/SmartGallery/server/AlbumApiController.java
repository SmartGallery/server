package info.susek.kk.SmartGallery.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import info.susek.kk.SmartGallery.server.model.Album;
import info.susek.kk.SmartGallery.server.model.User;
import info.susek.kk.SmartGallery.server.service.PhotoAlbumService;

@RequestMapping("/api")
@RestController
public class AlbumApiController {
	
	@Autowired
	private PhotoAlbumService photoAlbumService;

	@RequestMapping(value="/album", method = RequestMethod.GET )
	public List<Album> getAlbums (){
		
		List <Album> albums = photoAlbumService.getAlbums();
		
		return albums;
	}
	
	@RequestMapping(value="/album/{id}", method = RequestMethod.GET)
	public ResponseEntity<Album> getAlbum(@PathVariable("id") Long id){
		
		Album album = photoAlbumService.getAlbum(id);
		
		return new ResponseEntity<Album>(album, HttpStatus.OK);
	}
	
	
	
}
