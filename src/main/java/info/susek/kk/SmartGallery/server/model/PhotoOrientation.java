package info.susek.kk.SmartGallery.server.model;

public enum PhotoOrientation {

	PORTRAIT ("portrait"),
	LANDSCAPE("landscape");
	
	final String label;
	
	private PhotoOrientation(String label)
	{
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}
