package info.susek.kk.SmartGallery.server.model;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Repository;

@Repository
public class TeamDTO {


	@NotNull
	private long id;
	@NotNull
	private User admin;
	
	
	private Set<User> users = new HashSet<User>();
	
	@NotNull
	@Size(min=2, max=255)
	private String name;
	
	public TeamDTO() {
		
	}

	public TeamDTO(String name) {
		this.name = name;
	}
	
	public TeamDTO(Team team) {
		this.id = team.getId();
		this.admin = team.getAdmin();
		this.name = team.getName();
		this.users = team.getUsers();
		
	}
	
	
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Team getTeam() {
		
		
		Team team = new Team();
		team.setId(id);
		team.setAdmin(admin);
		team.setName(name);
		team.setUsers(users);
		users.size();
		return team;
	}

	/**
	 * @return the users
	 */
	public Set<User> getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	/**
	 * @return the admin
	 */
	public User getAdmin() {
		return admin;
	}

	/**
	 * @param admin the admin to set
	 */
	public void setAdmin(User admin) {
		this.admin = admin;
	}
	
	
}
