package info.susek.kk.SmartGallery.server.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import info.susek.kk.SmartGallery.server.model.Department;
import info.susek.kk.SmartGallery.server.model.Employee;
import info.susek.kk.SmartGallery.server.model.Team;
import info.susek.kk.SmartGallery.server.model.TeamDAO;
import info.susek.kk.SmartGallery.server.model.User;
import info.susek.kk.SmartGallery.server.model.UserDAO;
import info.susek.kk.SmartGallery.server.model.UserType;

@Service
public class UserService {

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private TeamDAO teamDAO;

	public List<User> getUsers() {

		List<User> userList = new ArrayList<User>();

		Iterable<User> daoUserList = userDAO.findAll();
		Iterator<User> daoUserIterator = daoUserList.iterator();

		while (daoUserIterator.hasNext()) {
			userList.add(daoUserIterator.next());
		}

		return userList;
	}

	public List<Team> teamList() {

		List<Team> teamList = new ArrayList<Team>();
		Iterable<Team> daoTeamList = teamDAO.findAll();
		Iterator<Team> daoTeamIterator = daoTeamList.iterator();

		while (daoTeamIterator.hasNext()) {
			teamList.add(daoTeamIterator.next());
		}

		return teamList;

	}

	public String getUserLogin(Long id) {
		User user = userDAO.findOne(id);
		return user.getLogin();

	}

	public void deleteUser(Long id) {
		userDAO.delete(id);

	}

	public void deleteTeam(Long id) {
		teamDAO.delete(id);

	}

	public User getUser(Long id) {

		return userDAO.findOne(id);
	}

	public void saveUser(User user) {

		if (user.getId() != 0 && user.getPassword().length() == 0) {
			user.setPassword(getUser(user.getId()).getPassword());
		}

		userDAO.save(user);

	}

	public void update(Long id, String login, String password, UserType type, int enabled) {
		User user;
		if (id == 0) {
			user = new User();
		} else {
			user = userDAO.findOne(id);
		}

		user.setLogin(login);
		user.setPassword(password);
		user.setType(type);
		user.setEnabled(enabled);

		userDAO.save(user);
	}

	public Team getTeam(Long id) {

		return teamDAO.findOne(id);
	}

	public void saveTeam(Team team) {

		teamDAO.save(team);
	}

	public List<User> getUsersWithChoose() {
		User newUser = new User();
		newUser.setId(-1);
		newUser.setLogin("choose user");
		List<User> newUserList = new ArrayList<User>();
		newUserList.add(newUser);
		newUserList.addAll(getUsers());
		return newUserList;
	}

	public void blockUser(User user) {

		if (user.getEnabled() == 1) {
			user.setEnabled(0);
			userDAO.save(user);
		}
	}

	public void unblockUser(User user) {

		if (user.getEnabled() == 0) {
			user.setEnabled(1);
			userDAO.save(user);
		}
	}

}
