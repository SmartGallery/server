package info.susek.kk.SmartGallery.server.model;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TeamDAO extends CrudRepository<Team, Long> {

}
