package info.susek.kk.SmartGallery.server.model;

public enum AlbumVisibility {

	PUBLIC("public"),
	PRIVATE("private"),
	GROUP("group"),
	HIDDEN("hidden");
	
	final String label;
	
	private AlbumVisibility(String label) {
		
		this.label = label;
	}
	
	private String getLabel(){
		return label;
	}
}
