package info.susek.kk.SmartGallery.server.model;

public enum BackupStatus {

	ENABLED("enabled",1),
	DISABLED("disabled",0);
	
	private final String key;
	private final Integer value;

	BackupStatus(String key, Integer value) {
	      this.key = key;
	      this.value = value;
	}

	public String getKey() {
	     return key;
	}
	public Integer getValue() {
	     return value;
	}
	
}
