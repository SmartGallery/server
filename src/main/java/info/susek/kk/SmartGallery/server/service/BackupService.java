package info.susek.kk.SmartGallery.server.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import info.susek.kk.SmartGallery.server.model.Backup;
import info.susek.kk.SmartGallery.server.model.BackupDAO;

@Service
public class BackupService {

	@Autowired
	BackupDAO backupDAO;

	public List<Backup> getBackups() {

		List<Backup> backupList = new ArrayList<Backup>();

		Iterable<Backup> daoBackupList = backupDAO.findAll();
		Iterator<Backup> daoBackupIterator = daoBackupList.iterator();

		while (daoBackupIterator.hasNext()) {
			backupList.add(daoBackupIterator.next());
		}

		return backupList;
	}

	public void deleteBackup(Long id) {

		backupDAO.delete(id);

	}

	public Backup getBackup(Long id) {

		Backup backup = backupDAO.findOne(id);

		return backup;
	}

	public void saveBackup(Backup backup) {

		backupDAO.save(backup);

	}

}
