package info.susek.kk.SmartGallery.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import info.susek.kk.SmartGallery.server.model.Department;
import info.susek.kk.SmartGallery.server.model.DepartmentDAO;
import info.susek.kk.SmartGallery.server.model.Employee;
import info.susek.kk.SmartGallery.server.model.EmployeeDAO;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDAO employeeDAO;
	@Autowired
	private DepartmentDAO departmentDAO;

	public Employee add(String employeeName, String departmentName) {

		Department d = new Department(departmentName);

		departmentDAO.save(d);

		Employee e = new Employee(employeeName, d);

		e = employeeDAO.add(e);

		return e;
	}

	// TODO: last to do: merge department
	public void compactDepartments() {

	}

	// TODO: next step: throw Exception when record not exist
	public Department getFirtsDepartmentByName(String departmentName) {

		Department department = departmentDAO.findFirstByNameOrderByIdAsc(departmentName);

		return department;
	}

	public List<Employee> allEmployees() {
		return employeeDAO.getAll();
	}

	public Employee getEmployee(Long id) {
		return employeeDAO.findOne(id);
	}

	public void deleteEmployee(Long id) {
		employeeDAO.deleteEmployee(id);
	}

	public Employee update(Long id, String name, Long departemntId) {
		Employee employee;
		if (id == 0) {
			employee = new Employee();
		} else {
			employee = employeeDAO.findOne(id);
		}

		employee.setName(name);
		employee.setDepartment(departmentDAO.findOne(departemntId));
		return employeeDAO.save(employee);

	}

	public void update(Employee employee) {
		employeeDAO.save(employee);

	}

	public List<Employee> allDepartmentEmployees(long id) {

		return employeeDAO.getByDepartment(id);
	}
}
