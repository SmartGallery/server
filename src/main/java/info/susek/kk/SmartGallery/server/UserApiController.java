package info.susek.kk.SmartGallery.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import info.susek.kk.SmartGallery.server.model.User;
import info.susek.kk.SmartGallery.server.service.UserService;

@RequestMapping("/api")
@RestController
public class UserApiController {

	@Autowired
	UserService userService;
	
	
	@RequestMapping(value="/users", method = RequestMethod.GET )
	public List<User> getUsers (){
		
		List <User> users = userService.getUsers();
		
		return users;
	}
	
	@RequestMapping(value="/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> getUser(@PathVariable("id") Long id){
		
		User user = userService.getUser(id);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	

	@RequestMapping(value="/user/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@PathVariable("id") Long id){
		
		User user = userService.getUser(id);
		HttpStatus status;
		String message = "";
		//Respo
		
		if(null != user){
			userService.deleteUser(id);
			status = HttpStatus.OK;
			message = "deleted";
			
		}else{
			status = HttpStatus.NOT_FOUND;
			message = "not found";
		}
		
		return new ResponseEntity<Object>(message, status);
	}
	
	
	
}
