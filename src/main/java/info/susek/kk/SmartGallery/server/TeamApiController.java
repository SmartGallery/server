package info.susek.kk.SmartGallery.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import info.susek.kk.SmartGallery.server.model.Team;
import info.susek.kk.SmartGallery.server.service.UserService;


@RequestMapping("/api")
@RestController
public class TeamApiController {

	@Autowired
	UserService userService;
	
	@RequestMapping(value="/teams", method = RequestMethod.GET)
	public ResponseEntity<List<Team>> getTeams (){
		
	List<Team> teamList = userService.teamList();
		
		return new ResponseEntity<List<Team>>(teamList, HttpStatus.OK);
	}
	

	@RequestMapping(value="/team/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getTeam (@PathVariable("id") Long id){
		
	Team team = userService.getTeam(id);
		
		return new ResponseEntity<Team>(team, HttpStatus.OK);
	}
	
	@RequestMapping(value="/team/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteTeam(@PathVariable("id") Long id){
		
		Team team = userService.getTeam(id);
		HttpStatus status;
		String message = "";
				
		if(null != team){
			userService.deleteTeam(id);
			status = HttpStatus.OK;
			message = "deleted";
			
		}else{
			status = HttpStatus.NOT_FOUND;
			message = "not found";
		}
		
		return new ResponseEntity<Object>(message, status);
	}
}
