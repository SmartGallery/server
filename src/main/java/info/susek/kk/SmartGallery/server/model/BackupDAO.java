package info.susek.kk.SmartGallery.server.model;

import org.springframework.data.repository.CrudRepository;

public interface BackupDAO extends CrudRepository<Backup, Long> {

}
