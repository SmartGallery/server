package info.susek.kk.SmartGallery.server.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Photo {

	@Id
	@GeneratedValue
	private long id;

	@JsonIgnore
	@ManyToOne
	private User user;
	private String file_name;
	private Date file_date;
	private String geotag;
	private String tags;
	private int resolution_x;
	private int resolution_y;
	private String description;
	private Date date;
	@Enumerated(EnumType.STRING)
	private PhotoOrientation orientation;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "AlbumPhoto", 
	joinColumns = {@JoinColumn(name = "photo_id")},
	inverseJoinColumns = {@JoinColumn(name = "album_id")})
	private Set<Album> albums = new HashSet<Album>();
	
	
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the file_name
	 */
	public String getFile_name() {
		return file_name;
	}
	/**
	 * @param file_name the file_name to set
	 */
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	/**
	 * @return the file_date
	 */
	public Date getFile_date() {
		return file_date;
	}
	/**
	 * @param file_date the file_date to set
	 */
	public void setFile_date(Date file_date) {
		this.file_date = file_date;
	}
	/**
	 * @return the geotag
	 */
	public String getGeotag() {
		return geotag;
	}
	/**
	 * @param geotag the geotag to set
	 */
	public void setGeotag(String geotag) {
		this.geotag = geotag;
	}
	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}
	/**
	 * @return the resolution_x
	 */
	public int getResolution_x() {
		return resolution_x;
	}
	/**
	 * @param resolution_x the resolution_x to set
	 */
	public void setResolution_x(int resolution_x) {
		this.resolution_x = resolution_x;
	}
	/**
	 * @return the resolution_y
	 */
	public int getResolution_y() {
		return resolution_y;
	}
	/**
	 * @param resolution_y the resolution_y to set
	 */
	public void setResolution_y(int resolution_y) {
		this.resolution_y = resolution_y;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the orientation
	 */
	public PhotoOrientation getOrientation() {
		return orientation;
	}
	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(PhotoOrientation orientation) {
		this.orientation = orientation;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the albums
	 */
	public Set<Album> getAlbums() {
		return albums;
	}
	/**
	 * @param albums the albums to set
	 */
	public void setAlbums(Set<Album> albums) {
		this.albums = albums;
	} 
}
