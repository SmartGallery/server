package info.susek.kk.SmartGallery.server.model;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface UserDAO extends CrudRepository<User, Long> {

	//User findFirstByNameOrderByIdAsc(String name);
}
