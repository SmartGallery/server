package info.susek.kk.SmartGallery.server.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import info.susek.kk.SmartGallery.server.model.User;
import info.susek.kk.SmartGallery.server.service.UserService;

@Component
public class StringToUserConventer implements Converter<Object, User> {

	@Autowired
	UserService userService;
	
	
	@Override
	public User convert(Object source) {
		
		Long id = Long.parseLong((String) source);
		User user = userService.getUser(id);
			
		return user;
	}

	
	
}
