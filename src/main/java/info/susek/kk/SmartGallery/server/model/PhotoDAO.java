package info.susek.kk.SmartGallery.server.model;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface PhotoDAO extends CrudRepository<Photo, Long> {
	
}
