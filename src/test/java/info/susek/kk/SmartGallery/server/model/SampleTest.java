package info.susek.kk.SmartGallery.server.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class SampleTest {

	@Test
	public void testSampleMetod() {
		Sample s = new Sample();
		
		assertEquals(3,	s.sampleMetod(1, 2));
	}

}
