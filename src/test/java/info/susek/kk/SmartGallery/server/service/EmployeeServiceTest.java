package info.susek.kk.SmartGallery.server.service;

import static org.junit.Assert.*;

import javax.transaction.Transactional;

import info.susek.kk.SmartGallery.server.model.Department;
import info.susek.kk.SmartGallery.server.model.DepartmentDAO;
import info.susek.kk.SmartGallery.server.model.Employee;
import info.susek.kk.SmartGallery.server.service.EmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/applicationContext.xml" )
public class EmployeeServiceTest {

	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	DepartmentDAO departmentDAO;

	@Test
	public void testAdd() {
		
		
		String employeeName = "Tom";
		String departmentName = "Management";
	/*	
		Employee employee = employeeService.add(employeeName, departmentName);
		
		assertEquals(employeeName, employee.getName());
		assertEquals(departmentName, employee.getDepartment().getName());
	*/
	
	}
	
	@Transactional
	@Test
	public void testGetFirtsDepartmentByName(){
		
		String departmentName = "Marketing";
		
		Department firtstDepartment = departmentDAO.save(new Department(departmentName));
		
		departmentDAO.save(new Department(departmentName));
		
		Department receivedDepartment = employeeService.getFirtsDepartmentByName(departmentName);
		
		assertEquals(firtstDepartment.toString(), receivedDepartment.toString());
		
	}
}
