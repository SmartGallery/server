# SmartGallery Sever application

This application is run on a computer where albums and photos should be stored

## Prerequisites

* MySQL database
* Wifi connections with mobile devices

## Run your application

To generate the start scripts simply run:

```
$ mvn package
```


Rename:

```
/src/main/webapp/WEB-INF/database.properties.sample
```

to:

```
/src/main/webapp/WEB-INF/database.properties
```

fill your database configuration.


And then simply run the script. On Mac and Linux, the command is:

```
$ sh target/bin/SmartGalleryServer
```

On Windows the command is:

```
C:/> target/bin/SmartGalleryServer.bat
```

That’s it. Your application should start up on port 8080. You can see it at http://localhost:8080

## Repository

[https://gitlab.com/SmartGallery/server](https://gitlab.com/SmartGallery/server)

## Credits

[Create a Java Web Application using Embedded Tomcat](https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat).

[Kurs Javy dla początkujących - Kobiety do kodu](http://kobietydokodu.pl/kurs-javy/).

[Maven and Hibernate tutorial](http://www.mastertheboss.com/jboss-frameworks/maven-tutorials/maven-hibernate-jpa/maven-and-hibernate-4-tutorial).

[Maven + (Spring + Hibernate) Annotation + MySql Example](https://www.mkyong.com/spring/maven-spring-hibernate-annotation-mysql-example/).